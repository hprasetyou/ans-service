<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;


require './vendor/autoload.php';

$app = new \Slim\App;

$key = 'hahahaha';

//==========================middleware==============================
$app->add(function ($request, $response, $next) {
  //retrive token, if not valid, reject request

    $key = 'hahahaha';
		$jwt = $request->getHeader('token');
		$decoded = JWT::decode($jwt[0], $key, array('HS256'));

    $request = $request->withAttribute('exam_code', $decoded->exam_code);
    $request = $request->withAttribute('nis', $decoded->nis);
  	$response = $next($request, $response);

  	return $response;
});
//==========================end middleware==========================




//=========================routing==================================

//test
$app->get('/test', function (Request $request, Response $response) {
    $exam_code = $request->getAttribute('exam_code');
    $response->getBody()->write(json_encode($exam_code));

    return $response;
});

//read file
$app->get('/assignment',function (Request $request, Response $response) {
    $exam_code = $request->getAttribute('exam_code');
    $nis = $request->getAttribute('nis');

    $dir = "./storage/assignment/".$exam_code;
    $file_path = $dir ."/file_".$nis.".json";
    //check if file exist
    if(file_exists($file_path)){
        //ambil rencana nomor dan acakan
        $output = json_decode(file_get_contents($file_path));
        $response = $response->withStatus(200);
    }else{
        $output = array('status'=>'error','message'=>'not found');
        $response = $response->withStatus(404);
    }
    $response = $response->withJson($output);

    return $response;
});

//write file
$app->post('/assignment',function (Request $request, Response $response) use($app) {
    $exam_code = $request->getAttribute('exam_code');
    $nis = $request->getAttribute('nis');

    $dir = "./storage/assignment/".$exam_code;
    $file_path = $dir ."/file_".$nis.".json";
    $exam_data = $request->getBody();

    if (!is_dir($dir)) {
        mkdir($dir,0777);
    }
    if(!file_exists($file_path)){
      //if file not exist write
      $file = fopen($file_path,"w");
      fwrite($file,$exam_data);
      fclose($file);
      $response = $response->withStatus(200);
      $response = $response->withJson(['status' => 'ok']);
    }else{
      $response = $response->withStatus(409);
      $response = $response->withJson(['status' => 'error','message'=>'file exist']);
    }

    return $response;
});

//replace file
$app->put('/assignment',function (Request $request, Response $response) {
    $exam_code = $request->getAttribute('exam_code');
    $nis = $request->getAttribute('nis');

    $dir = "./storage/assignment/".$exam_code;
    $file_path = $dir ."/file_".$nis.".json";
    $exam_data = $request->getBody();

    if (!is_dir($dir)) {
        mkdir($dir,0777);
    }

    $file = fopen($file_path,"w");
    fwrite($file,$exam_data);
    fclose($file);
    $response = $response->withStatus(200);
    $response = $response->withJson(['status' => 'ok']);

    return $response;
});


//===========================end route==================================




$app->run();
